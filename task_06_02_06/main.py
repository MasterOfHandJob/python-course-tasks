# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_06_02_06.
#
# Выполнил: Фамилия И.О.
# Группа: !!!
# E-mail: !!!


def foo(nums):
    """!!!

    Параметры:
        - nums (list): список.

    Сложность: !!!.
    """
    return (nums[0] + nums[-1] ** 2)
